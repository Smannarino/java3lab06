//settimio mannarino 1738233
import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random rand=new Random();

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
    }
    public int getWins() {
        return wins;
    }
    public int getTies() {
        return ties;
    }
    public int getLosses() {
        return losses;
    }
    public String playRound(String playerChoice){
        
        int getChoice=this.rand.nextInt(3);
        if(getChoice==0){
                if(playerChoice.equals("rock")){
                    this.ties++;
                    return "ai plays rock and its a tie game";
                }else if(playerChoice.equals("scissors")){
                    this.losses++;
                    return "ai plays rock and ai wins";
                }else{
                    this.wins++;
                    return "ai plays rock and player wins";
                }
        }else if(getChoice==1){
                if(playerChoice.equals("rock")){
                    this.wins++;
                    return "ai plays scissors and player won";
                }else if(playerChoice.equals("scissors")){
                    this.ties++;
                    return "ai plays scissors and its a tie game";
                }else{
                    this.losses++;
                    return "ai plays scissors and ai won";
                }
        }else if(getChoice==2){
                if(playerChoice.equals("rock")){
                    this.losses++;
                    return "ai plays paper and ai won";
                }else if(playerChoice.equals("scissors")){
                    this.wins++;
                    return "ai plays paper and player won";
                }else{
                    this.ties++;
                    return "ai plays paper and its a tie";
                }
        }else{
            return "invalid input";            
        }

    }
}

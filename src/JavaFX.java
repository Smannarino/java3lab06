//settimio mannarino 1738233

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class JavaFX extends Application {	
	private RpsGame game=new RpsGame();

	public void start(Stage stage) {
		Group root = new Group(); 
		VBox container=new VBox();
		HBox buttons=new HBox();

		Button rock=new Button("Rock");
		Button scissors=new Button("Scissors");
		Button paper=new Button("Paper");
		
		HBox textfileds=new HBox();

		TextField text=new TextField();
		text.setPrefWidth(200);
		TextField wins=new TextField("wins:0");
		TextField losses=new TextField("losses:0");
		TextField ties=new TextField("ties:0");

		buttons.getChildren().addAll(rock,scissors,paper);
		textfileds.getChildren().addAll(text,wins,losses,ties);
		container.getChildren().addAll(buttons,textfileds);
		root.getChildren().add(container);

		RpsChoice rockChoice=new RpsChoice(this.game, text, wins, losses, ties, "rock");
		rock.setOnAction(rockChoice);
		RpsChoice scissorChoice=new RpsChoice(this.game, text, wins, losses, ties, "scissors");
		scissors.setOnAction(scissorChoice);
		RpsChoice paperChoice=new RpsChoice(this.game, text, wins, losses, ties, "paper");
		paper.setOnAction(paperChoice);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

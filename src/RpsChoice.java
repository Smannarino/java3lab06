//settimio mannarino 1738233
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;



public class RpsChoice implements EventHandler<ActionEvent> {

    private RpsGame game=new RpsGame();
	private TextField message=new TextField();
	private TextField win=new TextField();
	private TextField losses=new TextField();
	private TextField tie=new TextField();
	private String playerChoice;

    public RpsChoice(RpsGame game,TextField message, TextField win,TextField losses, TextField tie, String playerChoice){
        this.game=game;
        this.message=message;
        this.win=win;
        this.losses=losses;
        this.tie=tie;
        this.playerChoice=playerChoice;
    }
    public void handle(ActionEvent e){
        String result=game.playRound(this.playerChoice);
        this.message.setText(result);
        int wins=this.game.getWins();
        int lost=this.game.getLosses();
        int ties=this.game.getTies();
        this.win.setText("wins:"+wins);
        this.losses.setText("losses:"+lost);
        this.tie.setText("ties:"+ties);
    }
    
}
